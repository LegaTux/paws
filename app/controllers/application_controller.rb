class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

  private

  def find_user(key, value)
    @user = User.find_by(key.to_s => value)
  end

  #Confirm if a user is logged in
  def logged_in_user?
    unless logged_in?
      store_location
      flash[:danger] = "Please log in first"
      redirect_to login_url
    end
  end

  def logged_in?
    !current_user.nil?
  end

  def array_pagination(collection, quantity)
    Kaminari.paginate_array(collection.first(quantity)).page(params[:page])
  end
end
