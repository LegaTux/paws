class UsersController < ApplicationController
  before_action :logged_in_user?, only: [:edit, :update, :show, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :find_user, only: [:show, :destroy]
  # Merge below line and edit destroy action to ensure only the current user can delete its own account to the site
  # before_action :correct_user, only: [:destroy]

  def show
    @feed_items = @user.posts.order('created_at DESC').page params[:page]
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if (@user.save)
      @user.send_activation_email
      flash[:info] = "A verification mail has been sent to your email address"
      redirect_to root_path
    else
      render('new')
    end
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = "Has actualizado mi perfil correctamente!"
      redirect_to @user
    else
      errors = @user.errors.full_messages
      flash[:danger] = "Unable to update the profile" + errors.first.to_s
      render('edit')
    end
    # render json: params
  end

  def destroy
    if current_user?(@user)
      @user.destroy
      flash[:warning] = "Sentimos mucho que hayas decidido marchar :("
      render 'signout'
    else
      @user.destroy
      flash[:success] = "El usuario se ha eliminado"
      redirect_to users_path
    end
  end

  private

  def user_params
    params.require(:user).permit(
      :pet_name,
      :human_name,
      :email,
      :pet_image,
      :human_image,
      :breed,
      :birthdate,
      :gender,
      :bio,
      :password,
      :password_confirmation
    )
  end

  def find_user
    @user = User.find(params[:id])
  end

  def correct_user
    @user = User.find(params[:id])
    unless current_user?(@user)
      flash[:warning] = "Solo puedes editar tu propio perfil"
      redirect_to(root_path)
    end
  end
end
