class PasswordResetsController < ApplicationController
    def edit
        @user = find_user('email', params[:email])
        if @user
          render 'edit'
        else
          flash[:info] = "We cannot find the url you're requesting"
          redirect_to root_url
        end
    end

    def create
        find_user('email', params[:password_reset][:email])
        if @user
            @user.create_password_reset_digest
            flash[:info] = "Email sent with password reset instructions"
            redirect_to root_url
        else
            flash.now[:danger] = "Email address not found"
            render 'new'
        end
    end

    def update
      find_user('email', params[:user][:email])
      if params[:user][:password].empty?
        @user.errors.add(:password, "can't be empty")
        render 'edit'
      elsif @user.update_attributes(user_params)
        log_in @user
        @user.update_attribute(:reset_digest, nil)
        flash[:success] = "Password has been reset."
        redirect_to @user
      else
        render 'edit'
      end
    end

   private

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end
 end
