class FriendsFinderController < ApplicationController
  def show
    @users = User.all.where.not(id: current_user.id).to_a
  end
end
