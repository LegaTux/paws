class AccountActivationsController < ApplicationController
    def edit
        find_user('email', params[:email])
        if user_activable(@user)
            @user.activate
            flash[:success] = "Account activated successfully! you can now log in"
            redirect_to login_path
        else
            flash[:danger] = "Invalid activation link"
            redirect_to login_path
        end
    end

    private

    def user_activable(user)
        user && !user.activated? &&
        user.authenticated?(:activation, params[:id])
    end

end
