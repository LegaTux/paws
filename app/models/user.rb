# TODO:40 Migrate to rails 5 for the action cable in the chat
# TODO:10 fix form validations on prod
# DONE:30 fix javascript on comments counter
# DONE:20 remove users link on dashboard

class User < ApplicationRecord
  # include PgSearch
  attr_accessor(:remember_token, :activation_token, :reset_token)
  # multisearchable :against => [:pet_name, :human_name, :breed]
  acts_as_voter

  # Active Record friendships (database friendships)
  has_many :posts, dependent: :destroy
  has_many :friendships,  class_name:   "Friendship",
                          foreign_key:  "user_id"
  has_many :friends, through: :friendships, source: :friend
  has_many :comments, dependent: :destroy
  has_many :notifications, dependent: :destroy

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\w\.[a-z]+\z/i

  before_save { @email = email.downcase }
  before_create :create_activation_digest

  # paperclip methods for handling avatar (profile pic) on users
  has_attached_file :pet_image,
                    styles: {
                      medium: "120x170^",
                      thumb: "60x60#",
                      small: "90x90#",
                      large: "300x300",
                      banner: "600x240#"
                    },
                    default_url: "default_pet_:style.jpg"
  has_attached_file :human_image,
                    styles: {
                      medium: "100x100",
                      thumb: "60x60#",
                      small: "90x90#",
                      large: "300x300"},
                    default_url: "default_human_:style.png"
  validates_attachment :pet_image, :human_image,
                        # presence: true,
                        content_type: { content_type: /\Aimage\/.*\Z/ },
                        size: { in: 0..5.megabytes }

  validates(:pet_name,  presence: true, length: { maximum: 30 })
  validates(:email, presence: true, length: { maximum: 255},
      format: {with: VALID_EMAIL_REGEX},
      uniqueness: {case_sensitive: false})

  has_secure_password
  validates(:password, presence: true, length: { minimum: 6 }, allow_nil: true)

  #This method returns the hash digest of the given string
  def self.digest(string)
    cost =  if ActiveModel::SecurePassword.min_cost
              BCrypt::Engine::MIN_COST
            else
              BCrypt::Engine.cost
            end
    BCrypt::Password.create(string, cost: cost)
  end

  # Remember a user n the database for use in persistent sessions.
  def remember
    @remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(@remember_token))
  end

  #Returns a random token, for security reasons. use base64
  def self.new_token
    SecureRandom.urlsafe_base64
  end

  #Returns true if the token is = to the digest, for secure remembering the logged User
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  #forgets a user
  def forget
    update_attribute(:remember_digest, nil)
  end

  #Activate the user account.
  def activate
    update_attribute(:activated, true)
    update_attribute(:activated_at, Time.zone.now)
  end

  #Send activation email
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  def create_password_reset_digest
    self.reset_token = User.new_token
    self.reset_digest = User.digest(reset_token)
    send_password_reset_email
  end

  # Password reset email
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  #Feed method for user posts
  def feed
    friend_ids = "SELECT friend_id FROM friendships
                     WHERE  user_id = :user_id"
    Post.where("user_id IN (#{friend_ids})
                     OR user_id = :user_id", user_id: id)
  end

  # Create relationship bond.
  def invite(other_user)
    friend_request = friendships.build(friend_id: other_user.id)
    friend_request.save
  end

  # Accept friendship invite
  def accept_friend(friendship)
    friendship.first.update_attribute(:request_accepted, true)
  end

  # Decline the friendship invite
  def reject_friend(friendship)
    Friendship.destroy(friendship.id)
  end

  # Unfriend a user.
  def unfriend(other_user)
    Friendship.destroy get_friendship(other_user)
    return !(requested? other_user)
  end

  # Returns true if the current user is friend of the other user.
  def is_friend?(other_user)
    friends.include?(other_user) || other_user.friends.include?(self)
  end

  #Cancels the notification if any was sent
  def remove_notification(other_user)
    notification = Notification.where("(user_id = :user_id AND
                                        notified_by_id = :id) OR
                                       (user_id = :id AND
                                        notified_by_id = :user_id)",
                                        id: id,
                                        user_id: other_user.id)
  end

  # Tells wether a friend request has been send
  def requested?(user)
    Friendship.where("user_id = :id AND friend_id = :user_id",
                      id: id, user_id: user.id).any?
  end

  # Look for a particular frienship if exists
  def get_friendship(other_user)
    me = friendships.where(friend_id: other_user.id)
    you = Friendship.where(user_id: other_user.id).where(friend_id: id)

    return me.size > 0 ? me : you
  end

  # Override ActiveRecord::friends
  def friends
    friendships = Friendship.where("(user_id = :user_id AND
                    request_accepted = :accepted) OR
                    (friend_id = :user_id AND
                    request_accepted = :accepted)",
                    user_id: id,
                    accepted: true)
    # Return an array with the list of friends if any or an empty array
    friendships.map { |f|
      User.where(id: f.user_id == id ? f.friend_id : f.user_id)
    }.first || []
  end

  private

  # Activation digest to be sent via email to activate new users
  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end

end
