module UsersHelper
    def avatar_url
        @user.avatar? ? @user.avatar.url(:medium) : "default.png"
    end
end
