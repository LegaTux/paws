# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Rails.env.development? || Rails.env.testing? || Rails.env.production?
  User.delete_all
  User.create!([{
                 pet_name:       'Jose Peña',
                 email:          'kenya@example.com',
                 password:       'password',
                 admin:              true,
                 activated:          true,
                 activated_at:       Time.zone.now
               }])

  99.times do |user|
    User.create!(
      pet_name:           Faker::Name.name,
      email:          "mail-#{user + 1}@example.org",
      password:       'password',
      admin:          false,
      activated:      true,
      activated_at:   Time.zone.now
    )
  end

  users = User.order(:created_at).take(6)
  50.times do
    content = Faker::Lorem.sentence(5)
    users.each { |user| user.posts.create!(content: content) }
  end

  # Friendhip seed
  users = User.all
  user  = users.first
  friends = users[2..50]
  friends.each do |friend|
    Friendship.create(user_id: user.id,
                      friend_id: friend.id,
                      request_accepted: true
                     )
  end
end
