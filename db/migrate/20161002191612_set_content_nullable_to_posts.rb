class SetContentNullableToPosts < ActiveRecord::Migration[5.0]
  def self.up
    change_column :posts, :content, :text, null: true
  end

  def self.down
    change_column :posts, :content, :text, null: false
  end
end
