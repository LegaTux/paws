class AddResetToUsers < ActiveRecord::Migration
  def change
    add_column :users, :reset_digest, :string
    add_column :users, :reset_sent_at, :datetime
    add_column :users, :human_name, :string
    add_column :users, :breed, :string
    add_column :users, :birthdate, :date
    add_column :users, :gender, :string
    add_column :users, :bio, :text
  end
end
