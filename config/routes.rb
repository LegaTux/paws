Rails.application.routes.draw do
  root             'posts#index'
  get 'signup'  => 'users#new'
  get 'login'   => 'sessions#new'
  get 'send_activation(:id)' => 'sessions#send_activation', as: 'send_activation'
  post 'login'  => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  resources :users, only: [:show, :update, :create, :destroy, :edit]

  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]

  resources :posts, only: [:index, :create, :destroy, :show] do
    resources :comments, except: :show
    get 'form', to: 'comments#show', as: 'comment_form'
    member do
      get 'like'
    end
  end
  resources :friendships, only: [:index], as: :friends
  post 'request_friend/:id', to: 'friendships#request_friend',   as: :request_friend
  post 'accept_friend/:id',  to: 'friendships#accept_friend',    as: :accept_friend
  post 'reject_friend/:id',  to: 'friendships#reject_friend',    as: :reject_friend
  post 'unfriend/:id',       to: 'friendships#unfriend',  as: :unfriend

  get 'notifications/:id/open_content', to: 'notifications#open_content',
                                        as: :open_content

  get 'notifications', to: 'notifications#index'

  get 'ffinder', to: 'friends_finder#show'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
