require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @user = users(:jose)
    @second_user = users(:marina)
  end

  test "should get new" do
    get :new
    assert_response :success
    assert_select "title", "Sign up | Paws!"
  end

  test "Should redirect out edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "Should redirect out update when not logged in" do
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "Should redirect edit when logged in as wrong user" do
    log_in_as(@second_user)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_path
  end
  
  test "Should redirect update when logged in as wrong user" do
    log_in_as @second_user
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert flash.empty?
    assert_redirected_to root_path
  end
end
